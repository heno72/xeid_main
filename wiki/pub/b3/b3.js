/**
  B3 : Simple blogging for PmWiki
  Written by (c) 2017-2020 Petko Yotov www.pmwiki.org/petko

  This text is written for PmWiki; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version. See pmwiki.php for full details
  and lack of warranty.

  Copyright 2017-2020 Petko Yotov www.pmwiki.org/petko
*/

(function(){
  var dates = document.getElementsByClassName('date'),
      myself = document.getElementById('b3datejs'), 
      locale, options = undefined, i, a, msec, d, ld, do_nothing;
  try { 
    options = JSON.parse(myself.getAttribute('data-options')); 
    if(options.hasOwnProperty('locale')){
      locale = options.locale;
      delete options.locale;
    }
  }
  catch(e) { options = false; }
  for(i=0; i<dates.length; i++) {
    a = dates[i].className.match(/stamp-(\d+)/);
    if (!a) continue;
    msec = parseInt(a[1])*1000;
    d = new Date(msec);
    dates[i].setAttribute('title', dates[i].innerHTML);
    ld = d.toLocaleString();
    if(options) {
      try { ld = d.toLocaleString( locale, options ); }
      catch(e) { do_nothing = 1; }
    }
    dates[i].innerHTML = ld;
  }
})();
