// MiniEdit v.20110829 by Petko Yotov http://notamment.fr
// built with Unverse.js by John Goodman http://unverse.net
if (typeof _.dRF == 'undefined') _.dRF = [];
function docReady(){ for(var i=0; i<_.dRF.length; i++) _.dRF[i](); }
_.top=function(e){ return e.offsetTop+((e.offsetParent)? _.top(e.offsetParent):0);}
_.left=function(e){ return e.offsetLeft+((e.offsetParent)? _.left(e.offsetParent):0);}
_.pos=function(e){return{X:_.left(e),Y:_.top(e),W:e.offsetWidth,H:e.offsetHeight};};
_.DD=function(a, b){
  a.DRP=b;
  _.dd(a, function(){
    var P=this.parentNode, ok=0, S=_.t(this.nodeName, P), c=_.pos(this), sp = document.createTextNode(' ');
    if(c.X || c.Y) {
      for(var i=0; i<S.length; i++){
        var e=S[i]; if(e==this) {continue;}
        var d=_.pos(e), I=(_.gs(e, 'display').indexOf('inline')>=0);
        if((c.X<=d.X && c.Y<=d.Y+d.H/2 && I) || (c.X<=d.X+d.W/2 && c.Y<=d.Y && !I)) {
          P.insertBefore(this, e); P.insertBefore(sp, e); ok=1; break; 
        }
      }
      if(!ok){P.appendChild(sp);P.appendChild(this); }
    }
    var ts = this.style; ts.position = ''; ts.top = ''; ts.left = ''; ts.display='';
    if(this.DRP) {this.DRP(this);}
  });
};
DBG=function(e){ if(typeof console == 'undefined')return;
  var r = "";
  switch (typeof e) {
    case 'string': case 'number': case 'boolean': r=e; break;
    default: for(var i in e) r+= i + ": "+e[i]+"\n";
  }
  if(typeof console != 'undefined')
    console.log(r);
  else
    alert(r);
};

_.Mdel=function(){
  var im = CIMG;
  CIMG.onmousedown = ''; CIMG.onmouseup = ''; CIMG.onmouseover = ''; CIMG.onmousemove = '';
  _.re(im, 'mouseover', _.tbx);
  _.re(im, 'mousedown', _.fadeout);
  _.re(im, 'mouseup', _.fadein);
  _.e(im, 'click', _.ma_click);
  im.style.cursor = 'crosshair';
 
  var is = _.t('IMG', 'miniall');
  var dv = is.length ? is[0].parentNode : _.$('miniall');
  _.aC(dv, CIMG, 1);
  _.relist();
  _.htbx();
};
_.Mmove=function(n){
  var P=CIMG.parentNode, S=_.t('IMG', P), cn=-1;
  for(var i=0; i<S.length; i++) { if(CIMG==S[i]){cn=i+n; break;} }
  if(0<=cn && cn<S.length){ var st=S[cn]; P.insertBefore(CIMG, st); P.insertBefore(document.createTextNode(' '), st); _.relist(CIMG); }
  _.htbx();
};
_.Medit=function(){
  _.s('me_ov');
  var t = prompt(this.title, CIMG.title);
  if(typeof t=='string') { CIMG.title=t; _.relist(CIMG); }
  _.h('me_ov');
};
var CIMG;
_.htbx=function(){ _.h('mdel'); _.h('medit'); _.h('mleft'); _.h('mright'); }
_.etarget=function(e){
  var targ;
  if (!e) var e = window.event;
  if (e.target) targ = e.target;
  else if (e.srcElement) targ = e.srcElement;
  if (targ.nodeType == 3) targ = targ.parentNode; // Safari bug
  return targ;
}
_.tbx=function(e){
  var im = _.etarget(e), p=_.pos(im), me=_.$('medit'), md=_.$('mdel'), d=_.pos(md), ml=_.$('mleft'), l=_.pos(ml), mr=_.$('mright'), r=_.pos(mr);
  CIMG = im;
  if(d.W*r.H*l.H*r.W==0) {_.s(me); _.s(md); _.s(ml); _.s(mr); d=_.pos(md);l=_.pos(ml);r=_.pos(mr);}
  _.s(me, p.X, p.Y);
  _.s(md, p.X+p.W-d.W, p.Y);
  _.s(ml, p.X, p.Y+p.H-l.H);
  _.s(mr, p.X+p.W-r.W, p.Y+p.H-r.H);
};
_.relist=function(){
  var v = "";
  _.all(_.t('IMG', 'miniedit'), function(im){
    v += im.getAttribute('rel')+'"'+im.title.replace(/"/g, '&quot;')+'"\n';
  });
  _.$('minilist').value = v;
  NsPreview = true;
};
_.opacity=function(e, o){var is=e.style; is.opacity=o/100.0; is.filter="opacity(alpha="+o+")";};
_.fadeout=function(e){var t= _.etarget(e); _.opacity(t, 70);};
_.fadein=function(e){var t= _.etarget(e); _.opacity(t, 100);};
_.me_init=function(im){
  _.DD(im, _.relist);
  _.e(im, 'mouseover', _.tbx); // _.e(im, 'mouseout', _.htbx); // flickers
  _.e(im, 'mousedown', _.fadeout);
  _.e(im, 'mouseup', _.fadein);
  ilist.push(im.getAttribute('rel'));
};
_.ma_init=function(im){
  var r = im.getAttribute('rel'), exists=0;
  for(var i=0; i<ilist.length; i++) if(ilist[i]==r) { dlist.push(im); exists=1; break;}
  if(! exists) _.e(im, 'click', _.ma_click);
};
_.ma_click = function(e){
  var im = _.etarget(e); _.re(im, 'click', _.ma_click);
  var is = _.t('IMG', 'miniedit');
  var dv = is.length ? is[0].parentNode : _.$('miniedit');
  _.aC(dv, im, 1);
  _.me_init(im);
  _.relist();
};
_.aC=function(p, c, sp){
  sp = sp || 0;
  if(sp)p.appendChild(document.createTextNode(' '));
  p.appendChild(c);
};
var ilist = [], dlist = [], NsPreview = false;
_.dRF.push(function(){
  var md = _.ce('mdel');  md.innerHTML = 'X'; md.onclick=_.Mdel;  md.title=_.v('ME_delete'); _.h(md);
  var me = _.ce('medit'); me.innerHTML = '?'; me.onclick=_.Medit; me.title=_.v('ME_edit'); _.h(me);
  var ml = _.ce('mleft'); ml.innerHTML = '&lt;'; ml.onclick=function(){_.Mmove(-1);}; _.h(ml);
  var mr = _.ce('mright'); mr.innerHTML = '&gt;'; mr.onclick=function(){_.Mmove(2);}; _.h(mr);
  var i=_.ce('me_ov'); i.style.width = _.dW() + 'px'; i.style.height = _.dH() + 'px'; _.h(i);

  _.all(_.t('IMG', 'miniedit'), _.me_init);
  _.all(_.t('IMG', 'miniall'),  _.ma_init);

  for(var i=0; i<dlist.length; i++) dlist[i].parentNode.removeChild(dlist[i]);
});

