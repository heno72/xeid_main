/**
	Maxi : an inline Picture Magnifier for PmWiki
	Written by (c) Petko Yotov 2009

	This script is POSTCARDWARE, if you like it or use it,
	please send me a postcard. Details at
	http://galleries.accent.bg/Cookbook/Postcard

	This text is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published
	by the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version. See pmwiki.php for full details
	and lack of warranty.

	Javascript support : Copyright 2009 Petko Yotov http://5ko.fr
	File modified 20090726
*/

function MaxiListen(evnt, elem, func)
{
	if (elem.addEventListener)  // W3C DOM
		elem.addEventListener(evnt,func,false);
	else if (elem.attachEvent)
		return elem.attachEvent("on"+evnt, func);
	else elem["on"+evnt] = func;
}

function MaxiStart2(idx)
{
	var imgs = document.images;
	if(imgs.length>0 )
	{
		for(var i=0; i<imgs.length; i++)
		{
			if(imgs[i].id != "maxi"+idx) continue;
			imgs[i].style.background = "url(" + MaxiData[idx].U + ") top left no-repeat" ;
			MaxiListen( "mouseover", imgs[i], MaxiOver );
			MaxiListen( "mouseout",  imgs[i], MaxiOut  );
			MaxiListen( "mousemove", imgs[i], MaxiMove );
			imgs[i].style.borderStyle = "outset";
			break;
		}
	}
}
function MaxiStart()
{
	var imgs = document.images;
	if(imgs.length>0 )
	{
		MaxiListen( "keydown", document, MaxiKeyPressed );
		MaxiListen( "keyup", document, MaxiKeyPressed );
	}
}
var CTRLdown=0, SHIFTdown=0;
function MaxiKeyPressed(e)
{
	if(typeof window.event!="undefined")e=window.event;
	if(e.type=="keydown")
	{ 
		if(e.keyCode==17) CTRLdown = 1;
// 		else if(e.keyCode==16) SHIFTdown = 1;
	}
	else if(e.type=="keyup")
	{
		if(e.keyCode==17) CTRLdown = 0;
// 		else if(e.keyCode==16) SHIFTdown = 0;
	}
}

function MaxiIDX(objid)
{
	return parseInt(objid.replace(/^maxi/, ''));
}

function MaxiInfo(idx)
{
	if( typeof(MaxiData[idx]) == 'undefined' ) return false;
	return MaxiData[idx];
}

function MaxiOver(e)
{
	if(CTRLdown==1) return;
	if(typeof window.event!="undefined")e=window.event;
	var target = MaxiTarget(e);

	var origwidth = target.width;
	var origheight = target.height;
	target.src = EmptyDot;
	target.width = origwidth;
	target.height = origheight;
}

function MaxiTarget(e)
{
	if(typeof(e.target)!= 'undefined' ) return e.target;
	if(typeof(e.srcElement)!= 'undefined' ) return e.srcElement;
	return false;
}

function MaxiOut(e)
{
	if(CTRLdown==1) return;
	if(typeof window.event!="undefined")e=window.event;
	var target = MaxiTarget(e);
	var info = MaxiInfo(MaxiIDX(target.id));
	target.src = info.img.src;
}


function MaxiX(e)
{
	if (typeof(e.layerX)!="undefined") return e.layerX -2;
	if (typeof(e.offsetX)!="undefined") return e.offsetX -2;
	return e.x - 1;
}

function MaxiY(e)
{
	if (typeof(e.layerY)!="undefined") return e.layerY-2;
	if (typeof(e.offsetY)!="undefined") return e.offsetY -2;
	return e.y - 1;
}

function MaxiMove(e)
{
	if(CTRLdown==1) return;
	if(typeof window.event!="undefined")e=window.event;
	var x = MaxiX(e);
	var y = MaxiY(e);
	
	if(x%ActiveGrid>0 && y%ActiveGrid>0) return;
	var target = MaxiTarget(e);
	var info = MaxiInfo(MaxiIDX(target.id));
	var bpx = Math.round(info.zx*x+MaxiPadding);
	var bpy = Math.round(info.zy*y+MaxiPadding);
	target.style.backgroundPosition = bpx + "px " + bpy +"px";
}


function MaxiEntry(w, h, W, H, u, U, cnt)
{
	this.w = w; this.h = h; this.u = u;
	this.W = W; this.H = H; this.U = U;
	this.img = new Image(w, h); this.img.src = u;
	this.IMG = new Image(W, H); this.IMG.src = U;
	this.IMG.onload = function(){ MaxiStart2(cnt); };
	this.zx = 1-(W+MaxiPadding*2)/w; this.zy = 1-(H+MaxiPadding*2)/h;
	return this;
}


