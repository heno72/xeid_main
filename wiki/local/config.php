<?php if (!defined('PmWiki')) exit();
##  This is a sample config.php file.  To use this file, copy it to
##  local/config.php, then edit it for whatever customizations you want.
##  Also, be sure to take a look at https://www.pmwiki.org/wiki/Cookbook
##  for more details on the customizations that can be added to PmWiki.

##  $WikiTitle is the name that appears in the browser's title bar.
$WikiTitle = 'Xenomancy';

##  $ScriptUrl is the URL for accessing wiki pages with a browser.
##  $PubDirUrl is the URL for the pub directory.
# $ScriptUrl = 'https://xenomancy.id/wiki/pmwiki.php';
## Experimenting with CleanUrl, so I'll just change the $ScriptUrl
$ScriptUrl = "https://xenomancy.id/wiki";
$PubDirUrl = 'https://xenomancy.id/wiki/pub';

##  If you want to use URLs of the form .../pmwiki.php/Group/PageName
##  instead of .../pmwiki.php?p=Group.PageName, try setting
##  $EnablePathInfo below.  Note that this doesn't work in all environments,
##  it depends on your webserver and PHP configuration.  You might also
##  want to check https://www.pmwiki.org/wiki/Cookbook/CleanUrls more
##  details about this setting and other ways to create nicer-looking urls.
$EnablePathInfo = 1;

## The above line is commented as I want to try the direction from:
## https://www.pmwiki.org/wiki/Cookbook/GetRidOfMain
## So this one could be isolated and deleted if it fails (horribly)
## Start of GetRidOfMain =======================================================
## add default group (Main) to page search path 
$PagePathFmt = array(
      '{$Group}.$1',           # page in current group
      '{$DefaultGroup}.$1',    # page in default group (Main)
      '$1.$1',                 # group home page
      '$1.{$DefaultName}',     # group home page
);
SDV($DefaultPage, 'Main.HomePage');
$pagename = MakePageName($DefaultPage, $pagename);

## reformat page urls to omit default group (Main)
$EnablePathInfo = 1;
$FmtPV['$PageUrl'] = 'PUE(($group==$GLOBALS["DefaultGroup"])
                            ? (($name==$GLOBALS["DefaultName"]) ? "$ScriptUrl/" : "$ScriptUrl/$name")
                              : "$ScriptUrl/$group/$name")';
$FmtP["!\\\$ScriptUrl/$DefaultGroup/!"] = '$ScriptUrl/';
## End of GetRidOfMain =========================================================

## To enable quiet redirect using (:redirect {=$FullName} quiet=1:) markup
$EnableRedirectQuiet = 1;

## For SiteGroup TalkTemplate, I haven't decided what template to use.
$TalkPageTemplate = 'Site.TalkTemplate';

## Not Saved Warning
$EnableNotSavedWarning = 1;

## Require edit authorization to view wikitext source and page history.
$HandleAuth['source'] = 'edit';
$HandleAuth['diff'] = 'edit';

## $PageLogoUrl is the URL for a logo image -- you can change this
## to your own logo if you wish.
#$PageLogoUrl = "$PubDirUrl/skins/pmwiki/pmwiki-32.gif";

## Apparently this is the cause of the problem
# $PageLogoUrl = "https://img.xenomancy.id/icons/android-chrome-512x512.png"

## If you want to have a custom skin, then set $Skin to the name
## of the directory (in pub/skins/) that contains your skin files.
## See PmWiki.Skins and Cookbook.Skins.
$Skin = 'vector';

## Use Adapt skin on iOS, Android, and other mobile
## But for now I want pmwiki-responsive instead.
if (preg_match('/Android|iPad|iPhone|iPod|Mobile|Phone|Kindle|Silk/',
$_SERVER['HTTP_USER_AGENT'])) $Skin='pmwiki-responsive';

## Switch Adapt Skin to a graphic logo
if ($GLOBALS["Skin"] == "adapt" ) {
$PageLogoUrl = "https://img.xenomancy.id/icons/xe-nomancy.svg"; # logo image
$AdaptWikiLogoEna = 1;
}

## For enlighten skin, change skincolor to brown
if ($GLOBALS["Skin"] == "enlighten" ) {
$PageLogoUrl = ''; # logo image
$WikiTag = "/web/wiki";
$SkinColor = 'brown';
#$WikiTitle = 'Xenomancy';
}

## For pmwiki, change page logo to this
if ($GLOBALS["Skin"] == "pmwiki" ) {
$PageLogoUrl = "https://img.xenomancy.id/icons/xe-nomancy.svg"; # logo image
}

## For pmwiki-responsive, change page logo to this
if ($GLOBALS["Skin"] == "pmwiki-responsive" ) {
$PageLogoUrl = "https://img.xenomancy.id/icons/xe-nomancy.svg"; # logo image
}

## For pmwiki-responsive, change page logo to this
if ($GLOBALS["Skin"] == "vector" ) {
$PageLogoUrl = "https://img.xenomancy.id/icons/xe-wiki.svg"; # logo image
$PageLogoAltUrl = "https://img.xenomancy.id/icons/xe-nomancy.svg";
}

## You'll probably want to set an administrative password that you
## can use to get into password-protected pages.  Also, by default
## the "attr" passwords for the PmWiki and Main groups are locked, so
## an admin password is a good way to unlock those.  See PmWiki.Passwords
## and PmWiki.PasswordsAdmin.
# $DefaultPasswords['admin'] = pmcrypt('secret');
$DefaultPasswords['admin'] = '$1$XwwF4OYr$YiA7xGbbERSmhI800MYzW0';
## So only logged in users can edit
$DefaultPasswords['edit'] = 'id:*';

## Unicode (UTF-8) allows the display of all languages and all alphabets.
## Highly recommended for new wikis.
include_once("scripts/xlpage-utf-8.php");
XLPage('en','PmWikiEn.XLPage');

## Authuser, for user-based authorization than a single password approach.
include_once("$FarmD/scripts/authuser.php");

## Enabling Sortable Tables
$EnableSortable = 1; # Enable sortable tables

## If you're running a publicly available site and allow anyone to
## edit without requiring a password, you probably want to put some
## blocklists in place to avoid wikispam.  See PmWiki.Blocklist.
# $EnableBlocklist = 1;                    # enable manual blocklists
# $EnableBlocklist = 10;                   # enable automatic blocklists

##  PmWiki comes with graphical user interface buttons for editing;
##  to enable these buttons, set $EnableGUIButtons to 1.
$EnableGUIButtons = 1;

##  To enable markup syntax from the Creole common wiki markup language
##  (http://www.wikicreole.org/), include it here:
# include_once("scripts/creole.php");

##  Some sites may want leading spaces on markup lines to indicate
##  "preformatted text blocks", set $EnableWSPre=1 if you want to do
##  this.  Setting it to a higher number increases the number of
##  space characters required on a line to count as "preformatted text".
# $EnableWSPre = 1;   # lines beginning with space are preformatted (default)
# $EnableWSPre = 4;   # lines with 4 or more spaces are preformatted
# $EnableWSPre = 0;   # disabled

##  If you want uploads enabled on your system, set $EnableUpload=1.
##  You'll also need to set a default upload password, or else set
##  passwords on individual groups and pages.  For more information
##  see PmWiki.UploadsAdmin.
# $EnableUpload = 1;f
# $DefaultPasswords['upload'] = pmcrypt('secret');
$EnableUpload = 1;
$UploadPermAdd = 0; # Recommended for most new installations
$UploadPrefixFmt = '/$Group/$Name'; # Because I used mini recipe, they recommended this configuration
$EnableUploadVersions=1;
$UploadMaxSize = 10*1024*1024; # limit uploaded files to 10MiB
$HandleAuth['upload'] = 'edit';

##  Setting $EnableDiag turns on the ?action=diag and ?action=phpinfo
##  actions, which often helps others to remotely troubleshoot
##  various configuration and execution problems.
# $EnableDiag = 1;                         # enable remote diagnostics

##  By default, PmWiki doesn't allow browsers to cache pages.  Setting
##  $EnableIMSCaching=1; will re-enable browser caches in a somewhat
##  smart manner.  Note that you may want to have caching disabled while
##  adjusting configuration files or layout templates.
# $EnableIMSCaching = 1;                   # allow browser caching

##  Set $SpaceWikiWords if you want WikiWords to automatically
##  have spaces before each sequence of capital letters.
# $SpaceWikiWords = 1;                     # turn on WikiWord spacing

##  Set $EnableWikiWords if you want to allow WikiWord links.
##  For more options with WikiWords, see scripts/wikiwords.php .
# $EnableWikiWords = 1;                    # enable WikiWord links

##  $DiffKeepDays specifies the minimum number of days to keep a page's
##  revision history.  The default is 3650 (approximately 10 years).
# $DiffKeepDays=30;                        # keep page history at least 30 days

## By default, viewers are prevented from seeing the existence
## of read-protected pages in search results and page listings,
## but this can be slow as PmWiki has to check the permissions
## of each page.  Setting $EnablePageListProtect to zero will
## speed things up considerably, but it will also mean that
## viewers may learn of the existence of read-protected pages.
## (It does not enable them to access the contents of the pages.)
# $EnablePageListProtect = 0;

##  The refcount.php script enables ?action=refcount, which helps to
##  find missing and orphaned pages.  See PmWiki.RefCount.
if ($action == 'refcount') include_once("scripts/refcount.php");

##  The feeds.php script enables ?action=rss, ?action=atom, ?action=rdf,
##  and ?action=dc, for generation of syndication feeds in various formats.
# if ($action == 'rss')  include_once("scripts/feeds.php");  # RSS 2.0
# if ($action == 'atom') include_once("scripts/feeds.php");  # Atom 1.0
# if ($action == 'dc')   include_once("scripts/feeds.php");  # Dublin Core
# if ($action == 'rdf')  include_once("scripts/feeds.php");  # RSS 1.0

##  By default, pages in the Category group are manually created.
##  Uncomment the following line to have blank category pages
##  automatically created whenever a link to a non-existent
##  category page is saved.  (The page is created only if
##  the author has edit permissions to the Category group.)
# $AutoCreate['/^Category\\./'] = array('ctime' => $Now);

##  PmWiki allows a great deal of flexibility for creating custom markup.
##  To add support for '*bold*' and '~italic~' markup (the single quotes
##  are part of the markup), uncomment the following lines.
##  (See PmWiki.CustomMarkup and the Cookbook for details and examples.)
# Markup("'~", "<'''''", "/'~(.*?)~'/", "<i>$1</i>");        # '~italic~'
# Markup("'*", "<'''''", "/'\\*(.*?)\\*'/", "<b>$1</b>");    # '*bold*'

##  If you want to have to approve links to external sites before they
##  are turned into links, uncomment the line below.  See PmWiki.UrlApprovals.
##  Also, setting $UnapprovedLinkCountMax limits the number of unapproved
##  links that are allowed in a page (useful to control wikispam).
# $UnapprovedLinkCountMax = 10;
# include_once("scripts/urlapprove.php");

##  The following lines make additional editing buttons appear in the
##  edit page for subheadings, lists, tables, etc.
# $GUIButtons['h2'] = array(400, '\\n!! ', '\\n', '$[Heading]',
#                     '$GUIButtonDirUrlFmt/h2.gif"$[Heading]"');
# $GUIButtons['h3'] = array(402, '\\n!!! ', '\\n', '$[Subheading]',
#                     '$GUIButtonDirUrlFmt/h3.gif"$[Subheading]"');
# $GUIButtons['indent'] = array(500, '\\n->', '\\n', '$[Indented text]',
#                     '$GUIButtonDirUrlFmt/indent.gif"$[Indented text]"');
# $GUIButtons['outdent'] = array(510, '\\n-<', '\\n', '$[Hanging indent]',
#                     '$GUIButtonDirUrlFmt/outdent.gif"$[Hanging indent]"');
# $GUIButtons['ol'] = array(520, '\\n# ', '\\n', '$[Ordered list]',
#                     '$GUIButtonDirUrlFmt/ol.gif"$[Ordered (numbered) list]"');
# $GUIButtons['ul'] = array(530, '\\n* ', '\\n', '$[Unordered list]',
#                     '$GUIButtonDirUrlFmt/ul.gif"$[Unordered (bullet) list]"');
# $GUIButtons['hr'] = array(540, '\\n----\\n', '', '',
#                     '$GUIButtonDirUrlFmt/hr.gif"$[Horizontal rule]"');
# $GUIButtons['table'] = array(600,
#                       '||border=1 width=80%\\n||!Hdr ||!Hdr ||!Hdr ||\\n||     ||     ||     ||\\n||     ||     ||     ||\\n', '', '',
#                     '$GUIButtonDirUrlFmt/table.gif"$[Table]"');

# Uncomment and change these if your server is not in your timezone
date_default_timezone_set('Asia/Jakarta'); # if you run PHP 5.1 or newer
# putenv("TZ=EST5EDT"); # if you run PHP 5.0 or older

$TimeFmt = '%d %B, %Y, at %I:%M %p %Z';

# use 'Template' in the current group if it exists, or the default group if it exists,
# otherwise use 'Main.MasterTemplate'
$EditTemplatesFmt = array('{$Group}.Template','{$DefaultGroup}.Template','Main.MasterTemplate');

## SkinChange included
$EnableAutoSkinList = 1;

## Favicon inclusion
$HTMLHeaderFmt['logo'] =
  '<link rel="apple-touch-icon" sizes="180x180" href="https://img.xenomancy.id/icons/apple-touch-icon.png?v=alJ9yEr3LX">
  <link rel="icon" type="image/png" sizes="32x32" href="https://img.xenomancy.id/icons/favicon-32x32.png?v=alJ9yEr3LX">
  <link rel="icon" type="image/png" sizes="16x16" href="https://img.xenomancy.id/icons/favicon-16x16.png?v=alJ9yEr3LX">
  <link rel="manifest" href="https://img.xenomancy.id/icons/site.webmanifest?v=alJ9yEr3LX">
  <link rel="mask-icon" href="https://img.xenomancy.id/icons/safari-pinned-tab.svg?v=alJ9yEr3LX" color="#5bbad5">
  <link rel="shortcut icon" href="https://img.xenomancy.id/icons/favicon.ico?v=alJ9yEr3LX">
  <meta name="msapplication-TileColor" content="#666666">
  <meta name="msapplication-config" content="https://img.xenomancy.id/icons/browserconfig.xml?v=alJ9yEr3LX">
  <meta name="theme-color" content="#666666">';

## Recipes should be down here.
# Custom GUI edit buttons
$GUIButtonDirUrlFmt = '$FarmPubDirUrl/skins/vector/guiedit';

# SectionEdit
include_once("cookbook/sectionedit.php");
$SectionEditHeaderLinkSpan = 1;
$SectionEditLinkText = "";

# HandyTableOfContents
include_once("cookbook/handytoc.php");
$HandyTocAnchorAfterElement = false;
$HandyTocDefaultTitle = "Contents";

# MediaCategories and DictIndex
include_once("cookbook/mediacat.php");
$AutoCreate['/^Category\./'] = array('ctime' => $Now, 'text' => $page['text']);
include_once("cookbook/titledictindex.php");

# HttpVariables
include_once("cookbook/httpvariables.php");
$FmtPV['$HTTPVariablesAvailable'] = true;

# MarkdownMarkupExtension
include_once("$FarmD/cookbook/markdownpmw.php");
$MarkdownMarkupMarkdownExtraEnabled = true;

# Automagical Pmwiki Embed
if($action=="browse" || $_REQUEST['preview']) {
  $Ape['dir'] = "$FarmD/pub/ape";
  $Ape['dirurl'] = '$FarmPubDirUrl/ape';
  $Ape['snippet'] = '<script...';
  $Ape['scripts'] = array('ape-local.js', 'ape.js');
  include_once("$FarmD/cookbook/ape.php");
}

# AMmathjax7
include_once("$FarmD/cookbook/AMmathjax7.php");

## Mini, miniedit, and Maxi gallery recipes
include_once("$FarmD/cookbook/mini.php");
$Mini['EnableLightbox'] = 1;
include_once("$FarmD/cookbook/maxi.php");
include_once("$FarmD/cookbook/miniedit.php");

## Cookbook/Footnotes
include_once("$FarmD/cookbook/footnote2.php");

## Cookbook/AddFootnote
include_once("$FarmD/cookbook/addfootnote.php");

## Cookbook/Toggle
include_once("$FarmD/cookbook/toggle.php"); 