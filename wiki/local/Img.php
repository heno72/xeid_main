<?php

## To maintain the feel of PmWiki zone, and to make it tidy for mobile,
## I will use pmwiki-responsive skin.
$Skin = 'photogallery';

$SkinColor = 'blue';

## For pmwiki-responsive, change page logo to this
if ($GLOBALS["Skin"] == "pmwiki-responsive" ) {
$PageLogoUrl = "https://img.xenomancy.id/icons/xe-nomancy.svg"; # logo image
}