<?php if (!defined('PmWiki')) exit();
## This is farmconfig.php, a global PmWiki configuration file for
## a collection ("farm") of wikis.

##  $FarmPubDirUrl is the URL for the farm-wide pub directory.
$FarmPubDirUrl = 'https://xenomancy.id/wiki/pub';

##  The refcount.php script enables ?action=refcount, which helps to
##  find missing and orphaned pages.  See PmWiki.RefCount.
if ($action == 'refcount') include_once("scripts/refcount.php");

##  If you want to use URLs of the form .../pmwiki.php/Group/PageName
##  instead of .../pmwiki.php?p=Group.PageName, try setting
##  $EnablePathInfo below.  Note that this doesn't work in all environments,
##  it depends on your webserver and PHP configuration.  You might also
##  want to check https://www.pmwiki.org/wiki/Cookbook/CleanUrls more
##  details about this setting and other ways to create nicer-looking urls.
$EnablePathInfo = 1;

## To enable quiet redirect using (:redirect {=$FullName} quiet=1:) markup
$EnableRedirectQuiet = 1;

##  PmWiki comes with graphical user interface buttons for editing;
##  to enable these buttons, set $EnableGUIButtons to 1.
$EnableGUIButtons = 1;

## SkinChange included
$EnableAutoSkinList = 1;

##  If you want uploads enabled on your system, set $EnableUpload=1.
##  You'll also need to set a default upload password, or else set
##  passwords on individual groups and pages.  For more information
##  see PmWiki.UploadsAdmin.
# $EnableUpload = 1;
# $DefaultPasswords['upload'] = pmcrypt('secret');
$EnableUpload = 1;
$UploadPermAdd = 0; # Recommended for most new installations
$EnableUploadVersions=1;
$UploadMaxSize = 10*1024*1024; # limit uploaded files to 10MiB
$HandleAuth['upload'] = 'edit';

## For SiteGroup TalkTemplate, I haven't decided what template to use.
$TalkPageTemplate = 'Site.TalkTemplate';

## Not Saved Warning
$EnableNotSavedWarning = 1;

## Require edit authorization to view wikitext source and page history.
$HandleAuth['source'] = 'edit';
$HandleAuth['diff'] = 'edit';

## Unicode (UTF-8) allows the display of all languages and all alphabets.
## Highly recommended for new wikis.
include_once("scripts/xlpage-utf-8.php");
XLPage('en','PmWikiEn.XLPage');

## Authuser, for user-based authorization than a single password approach.
#include_once("$FarmD/scripts/authuser.php");

## Enabling Sortable Tables
$EnableSortable = 1; # Enable sortable tables

# Uncomment and change these if your server is not in your timezone
date_default_timezone_set('Asia/Jakarta'); # if you run PHP 5.1 or newer
# putenv("TZ=EST5EDT"); # if you run PHP 5.0 or older

$TimeFmt = '%d %B, %Y, at %I:%M %p %Z';

# use 'Template' in the current group if it exists, or the default group if it exists,
# otherwise use 'Main.MasterTemplate'
$EditTemplatesFmt = array('{$Group}.Template','{$DefaultGroup}.Template','Main.MasterTemplate');

## Favicon inclusion
$HTMLHeaderFmt['logo'] =
  '<link rel="apple-touch-icon" sizes="180x180" href="https://img.xenomancy.id/icons/apple-touch-icon.png?v=alJ9yEr3LX">
  <link rel="icon" type="image/png" sizes="32x32" href="https://img.xenomancy.id/icons/favicon-32x32.png?v=alJ9yEr3LX">
  <link rel="icon" type="image/png" sizes="16x16" href="https://img.xenomancy.id/icons/favicon-16x16.png?v=alJ9yEr3LX">
  <link rel="manifest" href="https://img.xenomancy.id/icons/site.webmanifest?v=alJ9yEr3LX">
  <link rel="mask-icon" href="https://img.xenomancy.id/icons/safari-pinned-tab.svg?v=alJ9yEr3LX" color="#5bbad5">
  <link rel="shortcut icon" href="https://img.xenomancy.id/icons/favicon.ico?v=alJ9yEr3LX">
  <meta name="msapplication-TileColor" content="#666666">
  <meta name="msapplication-config" content="https://img.xenomancy.id/icons/browserconfig.xml?v=alJ9yEr3LX">
  <meta name="theme-color" content="#666666">';

## Recipes should be down here.
# Custom GUI edit buttons
$GUIButtonDirUrlFmt = '$FarmPubDirUrl/skins/vector/guiedit';

# SectionEdit
include_once("$FarmD/cookbook/sectionedit.php");
$SectionEditHeaderLinkSpan = 1;
$SectionEditLinkText = "";

# HandyTableOfContents
include_once("$FarmD/cookbook/handytoc.php");
$HandyTocAnchorAfterElement = false;
$HandyTocDefaultTitle = "Contents";

# MediaCategories and DictIndex
include_once("$FarmD/cookbook/mediacat.php");
$AutoCreate['/^Category\./'] = array('ctime' => $Now, 'text' => $page['text']);
include_once("$FarmD/cookbook/titledictindex.php");

# HttpVariables
include_once("$FarmD/cookbook/httpvariables.php");
$FmtPV['$HTTPVariablesAvailable'] = true;

# MarkdownMarkupExtension
include_once("$FarmD/cookbook/markdownpmw.php");
$MarkdownMarkupMarkdownExtraEnabled = true;

# Automagical Pmwiki Embed
if($action=="browse" || $_REQUEST['preview']) {
  $Ape['dir'] = "$FarmD/pub/ape"; # The directory (filesystem path) to the ape directory. Note the double quotes.
$Ape['dirurl'] = '$FarmPubDirUrl/ape'; # The URL-accessible directory where ape.js, leaflet.js and the other files can be downloaded by browsers. Note the single quotes.
$Ape['snippet'] = '<script...'; # The snippet inserted in the HTML page footer to load ape.js. The following variables will be replaced: {dirurl} with $Ape['dirurl'], {fname} with the file name, eg "ape.js", and {mtime} with the last modification timestamp for that file (useful for aggressive caching).
$Ape['scripts'] = array('ape-local.js', 'ape.js'); # a list of JS files to load; only those that actually exist in the pub/ape directory will be linked.
  include_once("$FarmD/cookbook/ape.php");
}

# AMmathjax7
include_once("$FarmD/cookbook/AMmathjax7.php");

## Mini, miniedit, and Maxi gallery recipes
include_once("$FarmD/cookbook/mini.php");
$Mini['EnableLightbox'] = 1;
include_once("$FarmD/cookbook/maxi.php");
include_once("$FarmD/cookbook/miniedit.php");

## Cookbook/Footnotes
include_once("$FarmD/cookbook/footnote2.php");

## Cookbook/AddFootnote
include_once("$FarmD/cookbook/addfootnote.php");

## Cookbook/Toggle
include_once("$FarmD/cookbook/toggle.php"); 