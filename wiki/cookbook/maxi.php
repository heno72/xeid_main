<?php if(!defined('PmWiki'))exit;
/**
  Maxi : an inline Picture Magnifier for PmWiki
  Written by (c) Petko Yotov 2009-2018

  This text is written for PmWiki; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version. See pmwiki.php for full details
  and lack of warranty.

  This text is partly based on the ThumbList2 and Mini picture galleries
  and on the PmWiki upload.php script.

  Copyright 2006-2018 Petko Yotov http://5ko.fr
  Copyright 2004-2007 Patrick R. Michaud http://www.pmichaud.com
*/
$RecipeInfo['Maxi']['Version'] = '20180602';

SDVA($Maxi, array(
  'ImgFmt' => '<img class="maxi" id="maxi%5$s" src="%1$s" title="%2$s" alt="%2$s"'
    . ' border="0" width="%3$s" height="%4$s" style="background:url(%1$s) 50% 50% no-repeat;" />',

  'MaxiCSS' => '<link rel="stylesheet" href="$FarmPubDirUrl/maxi/maxi.css" type="text/css" media="screen"/>',
  'LinkFmt' => '<a href="%2$s" class="maxilink">%1$s</a>',
  'ActiveGrid' => 1,'MaxiPadding' => 20,
  'MaxiDataFmt'=>'MaxiData[%d] = new MaxiEntry(%d, %d, %d, %d, "%s", "%s", %1$d);',
  'MaxiJS' => '<script type="text/javascript" src="$FarmPubDirUrl/maxi/maxi.js"></script>
<script type="text/javascript"><!--
  var ActiveGrid = %1$d;
  var MaxiPadding = %2$d;
  var EmptyDot = "$FarmPubDirUrl/maxi/empty.gif";
  var EmptyImg = new Image(); EmptyImg.src = EmptyDot;
  var MaxiData = new Array();

  //MaxiData//
  MaxiStart();
  //--></script>
'
));

Markup('Maxi:','<links',
    "/\\b([Mm]axi:)([^\\s\"\\|\\[\\]]+)(\"([^\"]*)\")?/",
    "mLinkMaxi");

function mLinkMaxi($m) {
  extract($GLOBALS["MarkupToHTML"]);
  return Keep(LinkMaxi($pagename,$m[1],$m[2],$m[4],$m[1].$m[2]),'L');
}

function LinkMaxi($PN, $imap, $path, $alt, $txt, $fmt=NULL) {
  global $FmtV, $UploadFileFmt, $UploadUrlFmt, $UploadPrefixFmt,
    $EnableDirectDownload, $Maxi, $HTMLHeaderFmt, $HTMLFooterFmt;

  static $cnt = -1; $cnt++;

  if (preg_match('!^(.*)/([^/]+)$!', $path, $m)) {
    $PN = MakePageName($PN, $m[1]);
    $path = $m[2];
  }
  list($small, $big) = explode(",", $path, 2);
  if($big=='') $big = preg_replace("/\\.\\w+$/", "-maxi$0", $small);
  if($big == $small) return "$imap$path";

  $uploadurl = FmtPageName(IsEnabled($EnableDirectDownload,1)
      ? "$UploadUrlFmt$UploadPrefixFmt/"
      : "\$PageUrl?action=download&upname=",
    $PN);

  $_big['name'] = MakeUploadName($PN, $big);
  $_big['path'] = FmtPageName("$UploadFileFmt/{$_big['name']}", $PN);
  if(!file_exists($_big['path'])) return LinkUploadMaxi($PN, $_big['name']);
  $_big['url'] = PUE("$uploadurl{$_big['name']}");
  list($_big['w'], $_big['h']) = getimagesize($_big['path']);


  $_small['name'] = MakeUploadName($PN, $small);
  $_small['path'] = FmtPageName("$UploadFileFmt/{$_small['name']}", $PN);
  if(!file_exists($_small['path'])) return LinkUploadMaxi($PN, $_small['name']);
  $_small['url'] = PUE("$uploadurl{$_small['name']}");
  list($_small['w'], $_small['h']) = getimagesize($_small['path']);

  if($_small['w']*$_small['h']> $_big['w']*$_big['h']) {
    $tmp = $_small; $_small = $_big; $_big = $tmp;
  }

  $out = sprintf($Maxi['ImgFmt'], str_replace('&', '&amp;', $_small['url']),
    $alt, $_small['w'], $_small['h'], $cnt);
  if($imap=='Maxi:')
    $out = sprintf($Maxi['LinkFmt'], $out, str_replace('&', '&amp;', $_big['url']));

  $HTMLHeaderFmt['maxi'] = $Maxi['MaxiCSS'];
  if(! isset($HTMLFooterFmt['maxi']) )
    $HTMLFooterFmt['maxi'] = sprintf($Maxi['MaxiJS'], $Maxi['ActiveGrid'], $Maxi['MaxiPadding'] );

  $mdata = sprintf($Maxi['MaxiDataFmt'], $cnt, $_small['w'], $_small['h'],
    $_big['w'], $_big['h'], $_small['url'],  $_big['url']);
  $HTMLFooterFmt['maxi'] = str_replace("//MaxiData//",
    "$mdata\n//MaxiData//", $HTMLFooterFmt['maxi']);

  return $out;
}

function LinkUploadMaxi($PN, $name) {
  global $FmtV, $LinkUploadCreateFmt;
  $FmtV['$LinkText'] = $name;
  $FmtV['$LinkUpload'] =
    FmtPageName("\$PageUrl?action=upload&amp;upname=$name", $PN);
  return FmtPageName($LinkUploadCreateFmt, $PN);
}
