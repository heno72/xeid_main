<?php if(!defined('PmWiki'))exit;
/**
  Visual editor for the Mini gallery
  Written by (c) 2011-2017 Petko Yotov pmwiki.org/petko

  This text is written for PmWiki; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version. See pmwiki.php for full details
  and lack of warranty.
*/
$RecipeInfo['MiniEdit']['Version'] = '20170820';

SDVA($HTMLStylesFmt, array(
  'miniedit'=>"span.miniedit { cursor: pointer; color: transparent; font-size: smaller; position: absolute; padding:.1em .5em; filter:alpha(opacity=0); z-index:2; } 
  span.miniedit:hover { color: black; background: #ddd; background: rgba(240, 240, 240, .5);filter:alpha(opacity=70);}"
));

SDVA($MiniEdit, array(
  'LbUrl' => '$FarmPubDirUrl/ultralightbox',
  'LbFiles' => array('unverse.js', 'miniedit.js', 'miniedit.css'),
  'ImgFmt' => '<img class="mini" src="%1$s" title="%2$s" alt="%2$s" rel="%4$s" border="0" />',
  'LinkFmt' => '%1$s',
  'MiniFmt' => '%1$s',
  'EnableCache'=> 0,
  'HideLinkIfNotAuth' => 0,
));
SDVA($HandleActions, array('miniedit'=>'HandleMiniEdit'));
SDVA($HandleAuth, array('miniedit'=>'edit'));

Markup('MiniEdit', '<Mini:', '/\\b[Mm]ini\\d?(?:_\\w+)?:/', 'InitMiniEdit');

function InitMiniEdit($m) {
  extract($GLOBALS["MarkupToHTML"]);
  $pn = $pagename;

  global $Mini, $MiniEdit, $action;
  static $has_run = 0;
  if($has_run || $action == 'miniedit') return $m[0]; 
  $has_run++;
  if($MiniEdit['HideLinkIfNotAuth']
    && ! RetrieveAuthPage($pn, 'edit', false, READPAGE_CURRENT)) return $m[0];
  
  foreach(preg_grep('/^MiniFmt/', array_keys($Mini)) as $k) {
    if(strpos($Mini[$k], "action=miniedit")===false) {
      $r = FmtPageName('<span class="miniedit" '
      .'onclick="self.location=\'{$PageUrl}?action=miniedit&amp;mid=%2$d\';" title="$[Edit gallery]">$[Edit gallery]</span>%1$s', $pn);
      $Mini[$k] = str_replace(array("%1\$s", "%s"), $r, $Mini[$k]);
    }
  }
  return $m[0];
}
function HandleMiniEdit($pagename, $auth='edit') {
  global $Mini, $MiniEdit, $HandleMiniEditFmt, $PageStartFmt, $PageEndFmt, $InputValues, $Author, $FmtV, $Now, $ChangeSummary, $MarkupTable;
  $mid = intval(@$_REQUEST['mid']);
  if (@$_POST['cancel'] || !$mid || (@$_POST['post']>'' && !@$_POST['minilist'])) { Redirect(FmtPageName('$FullName', $pagename)); return; }
  
  foreach($MiniEdit as $k=>$v) $Mini[$k]=$v;
  for($i=0; $i<=9; $i++) foreach(array("Img", "Link", "Mini") as $m) if(isset($Mini["{$m}Fmt$i"])) unset($Mini["{$m}Fmt$i"]);
  
  $page = RetrieveAuthPage($pagename, $auth, true);
  if(!$page) Abort("?cannot edit $pagename");
  
  MiniHeaderFmt();
  $tmp = MarkupToHTML($pagename, $page['text']);
  
  $markup = $Mini['SourceMarkup'][$mid];
  $allmarkup = $Mini['SourcePrefix'][$mid] . "*";
  
  if(strpos($page['text'], $markup)===false) {
    Abort("MiniEdit couldn't find the Mini set.");
  }
  
  $InputValues['author'] = $Author;
  
  if(@$_POST['post']>'') {
    $lst = trim(str_replace(array("\r", "<", ">"),array('', '&lt;', '&gt;'),stripmagic($_POST['minilist'])));
    $newmarkup = $Mini['SourcePrefix'][$mid] . "[=\n$lst\n=]";
    $new = $page;
    $parts = explode($markup, $page['text'], 2);
    $new['text'] = $parts[0].$newmarkup.$parts[1];
    $new['csum'] = $new["csum:$Now"] = $ChangeSummary = "MiniEdit: $mid";
    UpdatePage($pagename, $page, $new);
    Redirect($pagename);
  }
  
  $form = <<<EOF
!! $[Edit gallery] $mid / {\$FullName}
>>id=miniedit<<
$markup
>><<

(:input form action={\$PageUrl} method=post:)(:input hidden action miniedit:)(:input hidden n {\$FullName}:)(:input hidden mid $mid:)\\
(:input hidden ME_edit "$[Edit image caption]" id=ME_edit:)(:input hidden ME_delete "$[Remove image from gallery]" id=ME_delete:)\\
(:input hidden basetime $Now:)
$[Author]: (:input text author:) \\
(:input submit post "$[Save]":) \\
(:input submit cancel "$[Cancel]":)\\\\
(:input hidden minilist id=minilist:)
(:input end:)

!! $[Existing pictures] [=$allmarkup=]
$[Click on a picture to add it to the gallery.]
>>id=miniall<<
$allmarkup
>><<
EOF;
  $FmtV['$PageText'] = MarkupToHTML($pagename, $form);

  SDV($HandleMiniEditFmt,array(&$PageStartFmt, '$PageText', &$PageEndFmt));
  PrintFmt($pagename,$HandleMiniEditFmt);
}
