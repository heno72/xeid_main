<?php if (!defined('PmWiki')) exit();
/**
  AddFootnote - Turn selected text into a footnote
  Written by (c) Randy Brown 2015 
  
  Based on a (now obsolete) version of Petko's fixurl recipe, 
  which was based on GPLv2 insMarkup() by Patrick Michaud, pmichaud.com
  (NOTE 2017-11-06: THIS IS NO LONGER THE CASE)

  This text is written for PmWiki; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version. See pmwiki.php for full details
  and lack of warranty.
*/
# Version date

$RecipeInfo['AddFootnote']['Version'] = '2017-11-06';

global $GUIButtonDirUrlFmt;
$GUIButtons['addfootnote'] = array(9993,'[^','^]','$[Footnote]', '$GUIButtonDirUrlFmt/addfootnote.gif"$[Add a footnote: Selected text will become a footnote.]"');
