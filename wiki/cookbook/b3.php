<?php if (!defined('PmWiki'))exit;
/**
  B3 : Simple blogging for PmWiki
  Written by (c) 2017-2020 Petko Yotov www.pmwiki.org/petko

  This text is written for PmWiki; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version. See pmwiki.php for full details
  and lack of warranty.

  Copyright 2017-2020 Petko Yotov www.pmwiki.org/petko
*/
$RecipeInfo['B3']['Version'] = '20201019';

$PostConfig['B3Init'] = 40;
array_unshift($EditFunctions, 'B3CountComments');
SDVA($HandleActions, array(
  'b3rss'=>'HandleB3RSS',
  'b3talk'=>'HandleB3Talk',
));
XLSDV('en',array(
  'b3msg_author' => 'Please type your name at the Author: prompt.',
  'b3msg_draft' => 'The current post is not yet public - its publication date is in the future. Only editors can see it.',
  'b3msg_exists' => 'Attention, a page already exists with the same name, its text is open below.',
  'b3msg_newpost' => 'You are about to create a new blog entry. It will remain hidden from listings/RSS while its date is in the future. Comments can be "open", "moderated", "closed" or "disabled".',
));

SDVA($B3, array(
  'DirUrl' => '$FarmPubDirUrl/b3',
  'TimeFmt' => '%Y-%m-%d %H:%M %z', # needs to be parsable!
  'EnableComments' => 'Open', # Open | Closed | Moderated | Disabled
  'EnableCaptcha' => true,
  'EnablePageTrail' => true,
  'EnableListTrail' => true,
  'DateOptions' => array(
    'weekday' => 'long',
    'year'    => 'numeric',
    'month'   => 'long',
    'day'     => 'numeric',
    'hour'    => '2-digit',
    'minute'  => '2-digit'
  ),
  'EnableWikiMarkupInComments' => true,
  'NewPostDefaultDate' => 'Now + 60min',
  'DefaultTags' => 'Uncategorized',
  'DefaultText' => "Describe {title} here.\n\n\n\n",
  'PerPage' => 10,
  'PageCache' => array(),
  'RSSTitle' => '{$WikiTitle}: {$Title}',
  'pattern' => "(\\[\\[#b3\\]\\])(.*?)(\\[\\[#b3comments\\]\\])(.*?)(\\[\\[#b3end\\]\\]|$)",
  'TemplateNewPost' => '(:messages:)
[[#b3]]
Title: {title}
Author: {author}
Date: {date}
Tags: {tags}
Comments: {comments}

{defaulttext}


[[#b3comments]]
[[#b3end]]
',
  'TemplateNewComment' => '
* %{hidden}b3-comment{wikimarkup}% [={from}=]: %date%{now}%% [={comment}
=]
',
  'TemplateSubHeader' => '
(:div312 class=b3-subheader:)
{$B3Date} {$B3Author} {$B3Tags} ({$NbComments} $[comments])
(:div312end:)
',
  'TalkForm' => <<<EOF
(:input form action={\$PageUrl} method=post class="b3-talkform frame":)(:input hidden action b3talk:)\
(:input default request=1:)(:input default author "{\$Author}" :)
(:messages:)
''' $[Leave a reply] '''\\\\
$[Your name (required)]:\\\\
(:input text author placeholder="$[Your name (required)]" required=required:)\\\\
$[Your comment (required)]:\\\\
(:input textarea csum rows=5 cols=80 placeholder="$[Your comment (required)]" required=required:)\\\\
(:input checkbox wikimarkup 1 label="$[Your comment contains wikitext markup]":)\\\\
{captcha}(:input submit go "$[Add comment]":)
(:input end:)
EOF
,
  'TemplateCaptcha' => '(:if321 ! auth edit:) $[Enter value:] {$Captcha} (:input captcha class=captcha required=required placeholder=required:) (:if321end:)',

  'TemplatePageTrail' => "
(:div312 class=\"frame b3-trail\":)
%lfloat% &laquo; [[{older}|+]] %%  %rfloat% [[{newer}|+]]  &raquo; %% [[&lt;&lt;]]
(:div312end:)
",
  'TemplateIncludeComments' => "
(:div312 class=b3-comments id=b3-commentlist:)
(:if312 !equal '{\$NbComments}' '0':)''' {\$NbComments} $[comments on] \"{\$Title}\" '''(:if312end:)
{discussion}
(:div312end:)
",
  'TemplateIncludeIntro' => "
(:div4312 class='b3-intro{draft}':)
!!! [[{pagename}|+]]
(:include {pagename}#b3#b3end:)
[[#b3end]]
(:div4312end:)
",
  'TemplateListTrail' => "
(:div312 class=\"frame b3-trail\":)
%lfloat%  {older} %%  %rfloat% {newer} %% [[&lt;&lt;]]
(:div312end:)
",
  'PostPatterns' => array(
    "/\r/"   => '',
    '/\\(:/' => '( :',
    '/:\\)/' => ': )',
    '/\\$:/' => '$ :',
    '/(\\[)([@=])/' => '$1 $2',
    '/([@=])(\\])/' => '$1 $2',
    '/(\\[\\[#)(b3.*?\\]\\])/i' => '$1 $2',
    ),

    'StartRSS' => '<?xml version="1.0" encoding="{Charset}"?>
<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
  <atom:link href="{flink}?action=b3rss" rel="self" type="application/rss+xml" />
  <title>{ftitle}</title>
  <link>{flink}</link>
  <description>{fdescription}</description>
  <lastBuildDate>{fdate}</lastBuildDate>
',
    'EndRSS'   => '
  </channel>
</rss>
',
    'ItemRSS'=> '
    <item>
      <title>{ititle}</title>
      <link>{ilink}</link>
      <guid isPermaLink="true">{ilink}</guid>
      <dc:creator>{iauthor}</dc:creator>
      <description>{itext}</description>
      {itags}
      <pubDate>{idate}</pubDate>
    </item>
',
    'LinkRSS' => '<a type="application/rss+xml" href="{$PageUrl}?action=b3rss">$[RSS feed]</a>',
));


SDVA($B3, array(
  'HTMLHeaderFmt' => "<link rel='stylesheet' href='{$B3['DirUrl']}/b3.css' type='text/css' />",
  'HTMLFooterFmt' => "<script type='text/javascript' src='{$B3['DirUrl']}/b3.js' 
    id='b3datejs' data-options='".json_encode($B3['DateOptions'])."' async></script>",
  'TemplateNewPostForm' => <<<EOF
(:input form action={\$PageUrl} method=post class="b3-newpostform frame":)(:input hidden action b3new:)(:input hidden tags {tags}:)
$[New post title]:\\\\
(:input text b3title size=30 required=required placeholder="$[New post title]":)\\\\
$[Page name (optional)]:\\\\
(:input text b3name size=30 placeholder="$[Page name (optional)]":)\\\\
$[Date]:\\\\
(:input text b3date size=15 placeholder="$[Date]" value="{$B3['NewPostDefaultDate']}":) \\
(:input submit "" "$[Add post]":)
(:input end:)
EOF
));

# get $Author
include_once("$FarmD/scripts/author.php");

if ($B3['EnableCaptcha'] && file_exists("$FarmD/cookbook/captcha.php"))
  include_once("$FarmD/cookbook/captcha.php");
else $B3['EnableCaptcha'] = 0;
$CaptchaImageCSS = 'border: none; vertical-align: middle;';

SDVA($FmtPV, array(
  '$B3LinkRSS' => 'Keep(FmtPageName($GLOBALS["B3"]["LinkRSS"], "$group.$name"))',
  '$B3Author' => 'B3Vars($group, $name, "Author")',
  '$B3Date'   => 'B3Vars($group, $name, "Date")',
  '$B3Tags'   => 'B3Vars($group, $name, "Tags")',
  '$B3Public'   => 'B3Vars($group, $name, "Public")',
  '$NbComments' => '@$page["nbcomments"]? intval($page["nbcomments"]) : "0"',
  '$NbCommentsExt' => '@$page["nbcomments"]? $page["nbcomments"] : "0"',
));

SDV($QualifyPatterns["/(\\[\\[#b3\\]\\])(\n)/"], 'QualifyB3');

Markup('b3', '>nl1', "/{$B3['pattern']}/s", 'FmtB3');
Markup('b3-comment', '<b3', "/^(\\*+ *%)(((?:HIDDEN *)?b3-comment.*?)?% *(\\S.*?): *%date%(.*?)%%\\s*$KeepToken(\\d.*?)$KeepToken *)$/mi", 'FmtB3Comment');
Markup('b3-list', '<b3', "/\\(:b3-list(.*?):\\)/", 'FmtB3List');

function QualifyB3($m) {
  extract($GLOBALS["tmp_qualify"]);
  return $m[1].'PageName:'. $pagename.$m[2];
}
function FmtB3List($m) {
  global $B3, $action, $HTMLHeaderFmt, $HTMLFooterFmt, $Now;
  extract($GLOBALS["MarkupToHTML"]); # get $pagename
  $currentpage = $pagename;

  SDVA($HTMLHeaderFmt, array('b3css' => $B3['HTMLHeaderFmt']));
  SDVA($HTMLFooterFmt, array('b3js' => $B3['HTMLFooterFmt']));

  $opt = array_merge(array('tags'=>'*', 'author'=>'*'), ParseArgs($m[1]));

  if ($opt['pagename']) $pagename = $opt['pagename']; # included?

  $canedit = CondAuth($pagename, 'edit');
  $text = '';
  if ($opt['newpostform'] == 2 || ($canedit && $opt['newpostform'])) {
    $form = str_replace('{tags}', $opt['tags'], $B3['TemplateNewPostForm']);
    $text .= Qualify($pagename, $form);
  }

  $plist = SliceB3($pagename, $canedit, $opt);

  $text .= "\n";

  foreach ($plist['listnames'] as $pn) {
    $DRAFT = $B3['PageCache'][$pn]['draft'] ? ' DRAFT' : '';
    $text .= str_replace(
      array('{pagename}', '{draft}'),
      array($pn, $DRAFT),
      $B3['TemplateIncludeIntro']
    );
  }

  $hastrail = isset($opt['trail'])? $opt['trail'] : $B3['EnableListTrail'];
  if ($hastrail) {
    $pn = @$B3['ListSelfLink'] ? $currentpage : $pagename;
    
    $olderlink = $plist['olderpagenumber'] ? "&laquo; [[{$pn}?p={$plist['olderpagenumber']}| $[Older posts] ]]" : "";
    $newerlink = $plist['newerpagenumber'] ? "[[{$pn}?p={$plist['newerpagenumber']}| $[Newer posts] ]] &raquo;" : "";
    if ("$olderlink$newerlink") {
      $text .= str_replace(
        array('{older}', '{newer}'),
        array($olderlink, $newerlink),
        $B3['TemplateListTrail']
      );
    }
  }

  $HTMLHeaderFmt['b3rss'] = '<link rel="alternate" type="application/rss+xml"
    title="'.$B3['RSSTitle'].'" href="{$PageUrl}?action=b3rss" />';

  return PRR($text);
}

function FmtB3($m) {
# TODO: Comment-Delete/Approve/Hide

  list(, $bstart, $btext, $bcommline, $bcommlist, $bend) = $m;

  global $B3, $action, $Now, $EnablePostCaptchaRequired, $MessagesFmt, $HTMLFooterFmt, $HTMLHeaderFmt, $RecipeInfo;

  SDVA($HTMLHeaderFmt, array('b3css' => $B3['HTMLHeaderFmt']));
  SDVA($HTMLFooterFmt, array('b3js' => $B3['HTMLFooterFmt']));

  extract($GLOBALS["MarkupToHTML"]); # get $pagename
  list($group, $name) = explode('.', $pagename);
  $homepage = MakePageName($pagename, "$group.");

  $canedit = CondAuth($pagename, 'edit');

  $header = $B3["TemplateSubHeader"];

  list($text, $attr, $intro) = ParseHeadB3($btext);

  if (isset($attr['PageName'])) {
    $header = Qualify($attr['PageName'], $B3["TemplateSubHeader"]);
    return "$header\n$intro%more% [[{$attr['PageName']}| $[Read more...] ]]%%\n";
  }

  $draftclass = (strtotime($attr['Date'])>$Now) ? ' DRAFT' : '';
  if ($draftclass) {
    $publishlink = "<a href='{\$PageUrl}?action=edit&amp;b3publish=1&amp;preview=1' rel='nofollow'>$[Publish]</a>.";
    $MessagesFmt[] = "<div class='frame b3message'>$[b3msg_draft] $publishlink</div>";
    if(!$canedit) return "(:messages:)";
  }
//   xmp($header);
//   if ($EnablePostCaptchaRequired || $draftclass) $header = "\n(:messages:)\n$header";

  if (@$attr['Title'])
    PCache($pagename, array('title' => SetProperty($pagename, 'title', $attr['Title'], NULL, 1)));

  $trail = $inccomments = $form = "";

  if ($B3['EnablePageTrail']) {
    $plist = SliceB3($pagename, $canedit);
    $trail = str_replace(
      array('{older}', '{newer}'),
      array($plist['olderpagename'], $plist['newerpagename']),
      $B3['TemplatePageTrail']
    );
  }

  $bcommarg = strtolower( @$attr['Comments'] ? $attr['Comments'] : $B3['EnableComments'] );

  if ((! preg_match('!^(disabled)$!', $bcommarg)) && (! $draftclass)) {
    $inccomments = str_replace('{discussion}', $bcommlist, $B3['TemplateIncludeComments']);
  }

  if (preg_match('!^(open|moderated)$!', $bcommarg) && ! $draftclass) {
    $captcha = $B3['EnableCaptcha'] ? $B3['TemplateCaptcha'] : '';
    $form = str_replace(
      array('{captcha}'),
      array($captcha),
      $B3['TalkForm']
    );
  }

  $bottom = ("$trail$inccomments$form")
    ? "(:div312312 id=b3-after:)\n$trail\n$inccomments\n$form\n(:div312312end:)\n"
    : "";

  $share = @$B3['ShareMarkup'];
  $out = "(:div312312 class='b3-fullpost$draftclass':)(:nl:)$header\n$text\n(:div312312end:)$share\n$bottom";
  if (! strpos($out, '(:messages:)') ) $out = "(:nl:)(:messages:)\n$out";
  return PRR($out);
}

function ParseHeadB3($text) {
  global $action;
  $attrpat = "(Title|Author|Date|Tags|Status|Comments|PageName)";

  preg_match_all("/^$attrpat: *(.*?) *$/m", $text, $attrx, PREG_SET_ORDER);

  $attr = array();
  foreach ($attrx as $a) $attr[$a[1]] = trim($a[2]);

  $newtext = '';
  if ($text) while($newtext != $text) {
    $newtext = $text;
    $text = trim(preg_replace("/^\n?$attrpat:.*/", '', $text));
  }

  if (strpos($text, '[[#more]]')) list($intro,) = explode('[[#more]]', $text);

  else {
    list($intro, ) = preg_split('/^!/m', $text);

    if ($intro == $text ||  ! trim($intro) ) {
      list($intro, ) = preg_split("/(\n*<:vspace>|\n\n)/", $text, 2, PREG_SPLIT_NO_EMPTY);
    }
  }

  $intro = preg_replace("/(^(\n*<:vspace>\n)+|\n<:vspace>\n*$)/", '', $intro);
  $text  = preg_replace("/(^(\n*<:vspace>\n)+|\n<:vspace>\n*$)/", '', $text);

  return array(trim($text), $attr, trim($intro));
}

function MatchB3($pageattr, $searchp) {
  $pageattr = explode(',', $pageattr);
  $searchp = ParseArgs(str_replace(',', ' ', FoldB3($searchp, '\\w,\\x80-\\xfe*?')));

  if (isset($searchp['']))  $searchp['']  = implode(',', $searchp['']);
  if (isset($searchp['-'])) $searchp['-'] = implode(',', $searchp['-']);

  if (@$searchp['+']) foreach ($searchp['+'] as $p) {
    if (count(MatchNames($pageattr, $p))==0 ) return 0;
  }
  if (@$searchp['']) {
    if (count(MatchNames($pageattr, $searchp['']))==0) return 0;
  }
  if (@$searchp['-']) {
    if (count(MatchNames($pageattr, $searchp['-']))>0) return 0;
  }
  return 1;
}

function SliceB3($pagename, $canedit=false, $opt=array()) { #
  global $B3, $Now;
  CacheB3($pagename);
  list($group, $name) = explode('.', $pagename);
  $homepage = MakePageName($pagename, "$group.");

  $orderednames = array();

  foreach ($B3['PageCache'] as $k=>$a) {
    if ( (! $canedit) && $a['draft'] == 1 ) continue;
    if (@$opt['tags'] && ! MatchB3($a['tags'], $opt['tags'])) continue;
    if (@$opt['author'] && ! MatchB3($a['author'], $opt['author'])) continue;

    $orderednames[] = $k;
  }

  $flippednames = array_flip($orderednames);

  $olderpn = $newerpn = $homepage;
  $current = @$flippednames[$pagename];

  if ($current<count($flippednames)-1) # there is an older entry
    $olderpn = $orderednames[$current+1];

  if ($current>0) # there is a newer entry
    $newerpn = $orderednames[$current-1];

  $pagenumber = max(1, intval(@$_GET['p']));

  $PerPage = @$opt['perpage']? $opt['perpage'] : $B3['PerPage'];

  $currentslice = array();

  for ($i = ($pagenumber-1)*$PerPage; $i<$pagenumber*$PerPage&&$i<count($orderednames); $i++ ) {
    $currentslice[] = $orderednames[$i];
  }

  $olderpagenumber = ($pagenumber*$PerPage<count($orderednames)) ? $pagenumber+1 : 0;

  return array('listnames' => $currentslice, 'olderpagename'=>$olderpn, 'newerpagename'=>$newerpn,
    'newerpagenumber'=>$pagenumber-1, 'olderpagenumber'=>$olderpagenumber );
}

function HandleB3Talk($pagename, $auth = 'read') {
  global $B3, $ChangeSummary, $Now, $EnablePostCaptchaRequired, $IsPagePosted, $MessagesFmt, $action;

  $bpage = $new = RetrieveAuthPage($pagename, $auth, true);#, not READPAGE_CURRENT if we change the page
  if (! $bpage) Abort("? No permissions to $auth $pagename.");

  if ($B3['EnableCaptcha'] && ! CondAuth($pagename, "edit"))
    $EnablePostCaptchaRequired = 1;

  $r = MagicB3($_REQUEST);

  if (@$r['author']=='' || @$r['csum']=='' ) {
    $MessagesFmt[] = XL("Please fill the required form fields");
    $action = 'browse';
    HandleBrowse($pagename);
    exit;
  }

  $tpl = $B3['TemplateNewComment'];

  $now = strftime($B3['TimeFmt'], $Now);

  if (!preg_match('!^(.*?)'.$B3['pattern'].'(.*)$!s', $bpage['text'], $b3match))
    Abort("? $[This page does not appear to contain a b3 blog entry.]");

  list(, $pre_b, $bstart, $btext, $bcommline, $bcommlist, $bend, $post_b) = $b3match;

  $bcommarg = trim(PageVar($pagename, '$:Comments'));
  if (! @$bcommarg) $bcommarg = $B3['EnableComments'];
  $bcommarg = strtolower($bcommarg);

  if (! preg_match('!^(open|moderated)$!', $bcommarg))
    Abort("? $[This page does not currently accept comments.]");

  $hidden = ($bcommarg == 'moderated') ? 'HIDDEN ' : '';

  $wikimarkup = ($B3['EnableWikiMarkupInComments'] && @$r['wikimarkup']) ? ' wikimarkup' : '';

  $text = str_replace(
    array('{hidden}', '{wikimarkup}', '{now}',  '{from}',   '{comment}'),
    array( $hidden,    $wikimarkup,    $now,  $r['author'],  $r['csum']),
    $tpl
  );

  $new['text'] = rtrim("$pre_b$bstart$btext$bcommline$bcommlist"). "\n". trim($text)."\n" . ltrim("$bend$post_b");

  $new['csum'] = $new["csum:$Now"] = $ChangeSummary;
  UpdatePage($pagename, $bpage, $new);

  if ($IsPagePosted) Redirect($pagename, '$PageUrl#b3-commentlist');
  $action = 'browse';
  HandleBrowse($pagename);
}

function B3CountComments($pagename, &$page, &$new) {
  global $EnablePost;
  if (!$EnablePost) return;

  preg_match_all('/%.*?b3-comment.*?%/', $new['text'], $matches, PREG_SET_ORDER);
  $allcomments = count($matches);
  preg_match_all('/%(?:b3-comment.*?hidden.*?|HIDDEN b3-comment.*?)%/i', $new['text'], $matches, PREG_SET_ORDER);
  $hiddencomments = count($matches);
  $visiblecomments = $allcomments - $hiddencomments;
  $nbcomments = "$visiblecomments+$hiddencomments";

  if ($allcomments) $new['nbcomments'] = $nbcomments;
  else unset($new['nbcomments']);
}

function B3Vars($group, $name, $x) {
  global $Now, $TimeFmt;
  $pn = "$group.$name";
  $ptv = ($x == 'Public') ? "$:Date": "$:$x";
  $value = trim(PageVar($pn, $ptv));
  if (!$value) return "";
  if ($x == 'Date') {
    $stamp = strtotime($value);
    $date = strftime($TimeFmt, $stamp);
    return "%date stamp-$stamp%$date%%";
  }
  elseif ($x == 'Public') {
    $stamp = strtotime($value);
    return ($stamp>$Now) ? '' : 'public';
  }
  elseif ($x == 'Author') {
    return "%author% $[by] [[~$value]]%%";
  }
  elseif ($x == 'Tags') {
    $tags = explode(',', $value);
    $links = array();
    foreach ($tags as $tag) {
      $newpage = MakePageName($pn, $tag);
      if ($newpage)
        $links[] = "[[$newpage|+]]";
    }
    if (count($links)) return "%tags% $[in] " . implode(', ', $links)."%%";
  }
}

function B3Init($pagename) {
  global $B3, $action, $ROEPatterns, $Now, $Author, $MessagesFmt, $pagename, $ChangeSummary;

  if ($action == 'b3new') {
    $title = MagicB3($_REQUEST['b3title']);
    $name = preg_replace('![./]+!', ' ', MagicB3(@$_REQUEST['b3name']));
    if (! $name) $name = preg_replace('![./]+!', ' ', $title);
    $tags = MagicB3(@$_REQUEST['tags']);
    if ($tags == '*' || !$tags) $tags = $B3['DefaultTags'];
    $date = strtotime(MagicB3($_REQUEST['b3date']));
    if (! $date) $date = $Now;

    $dtext = $B3['DefaultText'];
    if(preg_match('/^page:(.*)$/', $dtext, $m)) {
      $dtext = IncludeText($pagename, $m[1]);
      $dtext = str_replace('<:vspace>', '', $dtext);
    }

    $action = "edit";
    $pagename = MakePageName($pagename, $name);
    if(! $pagename) Abort("$[Invalid page name]");

    if (!PageExists($pagename)) {
      $ROEPatterns['/\\{defaulttext\\}/'] = $dtext;
      $ROEPatterns['/\\{title\\}/'] = $title;
      $ROEPatterns['/\\{date\\}/'] = strftime($B3['TimeFmt'], $date);
      $ROEPatterns['/\\{comments\\}/'] = $B3['EnableComments'];
      $ROEPatterns['/\\{tags\\}/'] = $tags;
      $ROEPatterns['/\\{author\\}/'] = $Author ? $Author: XL('Your Name');

      $_POST['text'] = $B3['TemplateNewPost'];
      $MessagesFmt[] = "<div class='frame b3message'>$[b3msg_newpost]</div>";
      if (!$Author)
        $MessagesFmt[] = "<div class='frame b3alert'>$[b3msg_author]</div>";
    }
    else
      $MessagesFmt[] = "<div class='frame b3alert b3message'>$[b3msg_exists]</div>";
  }
  elseif ($action == 'b3talk') {
    $ChangeSummary =  substr('[b3] ' . XL("Comment posted") . ': '. $ChangeSummary, 0, 100);
  }
  elseif ($action == 'edit' && @$_REQUEST['b3publish']>'') {
    $ROEPatterns['/^ *Date *: *.*$/m'] = 'Date: '.strftime($B3['TimeFmt'], $Now);
  }
}

function MagicB3($x) {
  global $B3;
  if (is_array($x)) foreach ($x as $k=>$v) $x[$k] = MagicB3($v);
  else $x = trim(PPRA($B3['PostPatterns'], stripmagic($x)));
  return $x;
}

function FoldB3($x, $allowed='\\w,\\x80-\\xfe') {
  global $B3, $StrFoldFunction;
  SDV($StrFoldFunction, 'strtolower');

  $x = preg_replace("/[^$allowed]/", '', $x);
  $x = $StrFoldFunction($x);
  return $x;
}

function CacheB3($pagename, $retry = 0) {
  global $B3, $StrFoldFunction, $Now;
  SDV($StrFoldFunction, 'strtolower');

  if ((!$retry) && count($B3['PageCache'])) return;

  $B3['PageCache'] = array();

  list($group, $name) = explode('.', $pagename);

  $list = ListPages("$group.*,-*.RecentChanges,-*.GroupHeader,-*.GroupFooter,-*.GroupAttributes,-*-Talk");

  $cache = array();
  foreach ($list as $pn) {
    $date = trim(PageVar($pn, '$:Date'));
    $author = trim(PageVar($pn, '$:Author'));

    if ((!$date) || (!$author)) continue; # likely not a blog entry, or no permissions

    $title = trim(PageVar($pn, '$Title'));
    $tags = trim(PageVar($pn, '$:Tags'));

    @list($nbcomments, $hiddencomments) = explode('+', PageVar($pn, '$NbCommentsExt'));
    $nbcomments = intval($nbcomments);
    $hiddencomments = intval($hiddencomments);

    $draft = 0;

    $stamp = strtotime($date);

    if ($Now<$stamp) $draft = 1;

    $cache["$stamp $pn"] = array($pn => array('date'=>$date, 'stamp'=>$stamp, 'author'=>$author,
      'title'=>$title, 'tags'=>FoldB3($tags), 'nbcomments'=>$nbcomments, 'hcomments'=>$hiddencomments, 'draft'=>$draft));
  }

  krsort($cache);
  foreach ($cache as $a) {
    $B3['PageCache'] += $a;
  }
}

function FmtB3Comment($m) {
  global $B3, $action, $Now, $KeepToken;
  extract($GLOBALS["MarkupToHTML"]); # get $pagename

  static $hidden;
  if (!@$hidden) $hidden = CondAuth($pagename, 'edit') ? 'SHOW' : 'HIDE';

  $m[3] = strtolower($m[3]);

  if (strpos($m[3], 'hidden')!==false && $hidden=='HIDE') return '';

  $text = $GLOBALS['KPV'][$m[6]];
  $text = str_replace('<:vspace>', '', $text);

  if (strpos($m[3], 'wikimarkup')===false) $text = nl2br(trim($text));
  else $text = MarkupToHTML($pagename, $text);

  $GLOBALS['KPV'][$m[6]] = "<div class='b3-commenttext'>". $text."</div>";

  $from = $m[4];
  if (preg_match("!$KeepToken(\\d.*?)$KeepToken!", $from, $n)) {
    $GLOBALS['KPV'][$n[1]] = "<b>".$GLOBALS['KPV'][$n[1]]."</b>";
  }
  $stamp = strtotime($m[5]);
  $m[2] = str_replace('%date%', "%date stamp-$stamp%", $m[2]);
  return $m[1]."apply=item ".$m[2];
}

function HandleB3RSS($pagename, $auth = 'read') {
  global $B3, $Charset, $TimeISOZFmt, $RSSTimeFmt;

  $page = RetrieveAuthPage($pagename, $auth, false, READPAGE_CURRENT);
  if (!$page) Abort("? $[Cannot generate feed.]");

  if (!preg_match('!\\(:b3-list(.*?):\\)!', $page['text'], $m)) {
   Abort("? $[Page does not appear to be a B3 listing.]");
  }
  $ftitle = FmtPageName($B3['RSSTitle'], $pagename);
  $fdescription = PageVar($pagename, '$Description');
  if (!$fdescription) $fdescription = $pagename;

  $fstamp = intval($page['time']);
  $flink = PageVar($pagename, '$PageUrl');

  $canedit = false;
  $opt = array_merge(array('tags'=>'*', 'author'=>'*'), ParseArgs($m[1]));
  $plist = SliceB3($pagename, $canedit, $opt);

  $items = '';

  foreach ($plist['listnames'] as $pn) {
    $ipage = RetrieveAuthPage($pn, $auth, false, READPAGE_CURRENT);
    if (!$ipage) continue;

    if (!preg_match("/{$B3['pattern']}/s", Qualify($pn, $ipage['text']), $n)) continue;

    list(, $bstart, $btext, $bcommline, $bcommlist, $bend) = $n;

    list($text, $attr, $intro) = ParseHeadB3($btext);
    $share = Qualify($pn, @$B3['ShareMarkup']);
    $iintro = PHSC(MarkupToHTML($pn, $intro), ENT_NOQUOTES);
    $itext = PHSC(MarkupToHTML($pn, "$text\n\n$share"), ENT_NOQUOTES);

    $ititle = PHSC( PageVar($pn, '$Title') , ENT_NOQUOTES);
    $iauthor = PHSC($attr['Author'], ENT_NOQUOTES);
    $istamp = strtotime($attr['Date']);
    $idate = date('r', $istamp);
    $ilink = PageVar($pn, '$PageUrl');

    $tags = preg_split('! *,+ *!', $attr['Tags'], -1, PREG_SPLIT_NO_EMPTY);
    $itags = '';
    foreach ($tags as $t) $itags .= "<category>$t</category>\n";

    $items .= str_replace(
      array('{ititle}','{ilink}','{iauthor}','{iintro}','{itext}','{itags}','{idate}'),
      array( $ititle,   $ilink,   $iauthor,   $iintro,   $itext,   $itags,   $idate  ),
      $B3['ItemRSS']
    );
    $fstamp = max($istamp, $fstamp);
  }
  $fdate = date('r', $fstamp);

  $out = str_replace(
    array('{ftitle}', '{flink}', '{fdescription}', '{fdate}', '{Charset}'),
    array( $ftitle,    $flink,    $fdescription,    $fdate,    $Charset ),
    $B3['StartRSS']
  );

  $out .= $items . $B3['EndRSS'];
  $out = str_replace(array('&lt;', '&gt;', '&amp;'), array('&#x3C;', '&#x3E;', '&#x26;'), $out);

  header("Content-Type: application/rss+xml; charset=$Charset");
  die($out);
}

