<?php if (!defined('PmWiki')) exit();
/*
ASCIIMath - Display MathML rendered ascii formula into PmWiki 2.x pages
Using MathJax
Author: Massimiliano Vessi
Email: angerangel@gmail.com
Can you improve this code?  YES.
Updated for PHP7 by Peter Kay pkay42@gmail.com
 */
#version
$RecipeInfo['AMmathjax']['Version'] = '20190619';

#header to include with link to last version of MathJax
$HTMLHeaderFmt['MathJax'] =   "<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML-full' async></script>" ;
# (previous version was at http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_HTMLorMML-full)

#Markup substitution
Markup('{$', '>[=', '/\\{\\$(.*?)\\$\\}/', function ($m) { return Keep("`{$m[1]}`");});
  
#Added button, if is missing add  the image http://www.pmichaud.com/pmwiki/pub/guiedit/math.gif .
#to your your folder  pmwiki/pub/guiedit/  
SDV($GUIButtons['math'], array(1000, '{$ ', ' $}', '+-sqrt(n)', 

  '$FarmPubDirUrl/guiedit/math.gif"$[Math formula (ASCIIMathML)]"'));
  
